#ifndef __PIN_MAP_H__
#define __PIN_MAP_H__

// ESP32C3
#define _C3_U1TX        (0)
#define _C3_U1RX        (1)
#define _C3_SCL0        (5)
#define _C3_SDA0        (4)
#define _C3_SPI2_MISO   (10)
#define _C3_SPI2_MOSI   (3)
#define _C3_SPI2_SCLK   (2)

// ESP32S3
#define _S3_U1TX        (17)
#define _S3_U1RX        (18)
#define _S3_U2TX        (33)
#define _S3_U2RX        (34)
#define _S3_SCL0        (5)
#define _S3_SDA0        (4)
#define _S3_SCL1        (7)
#define _S3_SDA1        (6)
#define _S3_SPI2_MISO   (11)
#define _S3_SPI2_MOSI   (12)
#define _S3_SPI2_SCLK   (13)
#define _S3_SPI3_MISO   (8)
#define _S3_SPI3_MOSI   (9)
#define _S3_SPI3_SCLK   (10)

#endif