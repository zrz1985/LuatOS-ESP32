# API文档



| 模块                                                         | 描述             |
| ------------------------------------------------------------ | ---------------- |
| [adc](luat_lib_adc.md)                                       | 数模转换         |
| [crypto](luat_lib_crypto.md)                                 | 加解密和hash函数 |
| [gpio](luat_lib_gpio.md)                                     | GPIO操作         |
| [i2c](luat_lib_i2c.md)                                       | I2C操作          |
| [log](luat_lib_log.md)                                       | 日志库           |
| [pwm2](luat_lib_pwm2.md)                                     | PWM模块          |
| [rtos](https://gitee.com/openLuat/LuatOS/blob/1f811a65c31f1d7f7c6bf2d34268317d1af687c0/docs/api/lua/luat_lib_rtos.md) | RTOS底层操作库   |
| [spi](luat_lib_spi.md)                                       | spi操作库        |
| [uart](lua/luat_lib_uart.md)                                 | 串口操作库       |
| [wlan](lua/luat_lib_wlan.md)                                 | wifi操作库       |
| [esp32](luat_lib_esp32.md)                                   | esp32专用库      |

