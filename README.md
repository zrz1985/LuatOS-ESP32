# LuatOS For ESP32

[![esp32c3](https://github.com/dreamcmi/LuatOS-ESP32/actions/workflows/commit-build-c3.yml/badge.svg?branch=master)](https://github.com/dreamcmi/LuatOS-ESP32/actions/workflows/commit-build-c3.yml)[![esp32s3](https://github.com/dreamcmi/LuatOS-ESP32/actions/workflows/commit-build-s3.yml/badge.svg?branch=master)](https://github.com/dreamcmi/LuatOS-ESP32/actions/workflows/commit-build-s3.yml)

## 1、简介

尝试给ESP32适配一个[LuatOS](https://gitee.com/openLuat/LuatOS)，只是为了学习，个人项目，希望有更多的朋友来一起贡献。

目前C3基于ESP-IDF v4.3.2版本，S3基于 ESP-IDF release/v4.4 分支

当前分支ESP32C3/ESP32S3，为主力版本。

本项目仓库只有两个： [Gitee](https://gitee.com/dreamcmi/LuatOS-ESP32)  [Github](https://github.com/dreamcmi/LuatOS-ESP32) 

如果你在其他仓库发现本仓库代码均为非本人授权许可。

## 2、参与方式

[UserGuide-VSC](./doc/userguide.md)

[UserGuide-Clion](./doc/UserGuideCilon.md)

欢迎提issue和pr帮助项目进步。

ESP32开发QQ群：667220396

LuatOS开发QQ群：1061642968

## 3、相关文档

[Support](./doc/SUPPORT.md)

[ESP32C3解锁GPIO11](./doc/VDD_SPI_AS_GPIO.md)

进度更新中，欢迎提需求。

## 4、鸣谢

排名不分前后

- [LuatOS](https://gitee.com/openLuat/LuatOS) ：合宙LuatOS是运行在嵌入式硬件的实时操作系统,用户编写lua代码就可完成各种功能

- [ESPRESSIF ](https://www.espressif.com/): 乐鑫科技官网，提供高性价比高可玩性的ESP32系列产品

- [Wendal](https://gitee.com/wendal) ：技术大佬，LuatOS领头人

## 5、LICENSE

本项目遵循 Apache License 2.0 开源。

